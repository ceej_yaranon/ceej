"""
Arithmetic Game v1.2
"""

import random, datetime, time

listahan = []
score = {}

def rands():
    return random.randrange(1,9,1)  #includes 1 to 9

def rands2():
    return random.randrange(10,99,1)    #includes 10 to 99

def rands3():
    return random.randrange(100,999,1)  #includes 100 to 999

def rands4():
    return random.randrange(2,9,1)  #includes 2 to 9

def register(arg1, arg2, arg3):

    score = {'username': arg1, 'errors': arg2, 'time': arg3}

    listahan.append(score)
    return score

def check_random_for_division(arg1, arg2):
    temp = arg1 % arg2
    if temp is not 0:
        return False
    else:
        return True

def compute_time(arg):
    mins = arg/60
    secs = (arg%60)*60

    #print "Time: {}:{}".format(mins, secs)
    return (mins, secs)

def sort_dictionary():
    for a in listahan:
        for b in listahan.len()-1:
            if listahan[b]['score'] > listahan[b+1]['score']:
                listahan[b]['score'], listahan[b+1]['score'] = listahan[b+1]['score'], listahan[b][score]
    return listahan


def game(arg1, arg2):
    right = 0
    wrong = 0
    while right is not 10 or wrong is not 10:
        
        rand = random.randrange(1,5,1)

        #modeled from switch statement

        if rand == 1:   #Addition
            num1 = rands2()
            num2 = rands2()

            ans_user = input("Addition: {} + {} = ".format(num1, num2))
            ans_adds = num1 + num2

            if (ans_user != ans_adds):
                wrong += 1
            else:
                right += 1
            print "Answer: {}".format(ans_adds)
            print "{} - {}".format(right, wrong)

        elif rand == 2: #Subtraction
            num1 = rands2()
            num2 = rands2()

            if num1 > num2:
                ans_subs = num1 - num2
                ans_user = input("Subtraction: {} - {} = ".format(num1, num2))
            else:
                ans_subs = num2 - num1
                ans_user = input("Subtraction: {} - {} = ".format(num2, num1))

            if (ans_user != ans_subs):
                wrong += 1
            else:
                right += 1
            print "Answer: {}".format(ans_subs)
            print "{} - {}".format(right, wrong)

        elif rand == 3: #Multiplication
            num1 = rands2()
            num2 = rands4()

            ans_user = input("Multiplication: {} * {} = ".format(num1, num2))
            ans_mults = num1 * num2
    
            if ans_mults != ans_user:
                wrong += 1
            else:
                right += 1
            print "Answer: {}".format(ans_mults)
            print "{} - {}".format(right, wrong)

        elif rand == 4: #Division
            num1 = rands3()
            num2 = rands4()

            while check_random_for_division(num1, num2) is not True:
                num1 = rands3()
                num2 = rands4()
                check_random_for_division(num1, num2)

            ans_user = input("Division: {} / {} = ".format(num1, num2))
            ans_divs = num1 / num2

            if ans_divs != ans_user:
                wrong += 1
            else:
                right += 1

            print "Answer: {}".format(ans_divs)
            print "{} - {}".format(right, wrong)

        else:           #switch statement base case
            print rando

        if (right is 10 or wrong is 10):
            break
            
    tock = datetime.datetime.now()
    gametime = (arg2 - tock).seconds

    mins, secs = compute_time(gametime)
    
    if wrong is 10:
        print "\nUsername: {}".format(arg1)
        print "Time: {}:{}".format(mins, secs)
        print "Score: {}".format(right)
        print "Errors: {}\n".format(wrong)
    else:
        print "\nUsername: {}".format(arg1)
        print "Time: {}:{}".format(mins, secs)
        print "Score: {}".format(right)
        print "Errors: {}\n".format(wrong)

        register(arg1, wrong, gametime)

    main()

#program starts here
def main():
    uname = raw_input("Enter username: ")

    if uname is "":
            #display top 10 users in sorted order
            print sort_dictionary()
    else:
        start = raw_input("Start game? [y/n]: ")

        if start == 'y':
            print "Let's Go!"
            tick = datetime.datetime.now()
            game(uname, tick)
        else:
            raise SystemExit()

main()
gamecount = 0